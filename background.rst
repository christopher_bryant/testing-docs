Background
==========
|RST|_ is a little annoying to type over and over, especially
when writing about |RST| itself, and spelling out the
bicapitalized word |RST| every time isn't really necessary for
|RST| source readability.

.. |RST| replace:: reStructuredText
.. _RST: http://docutils.sourceforge.net/rst.html


More Background
-----------------
.. image:: pic.png
   :width: 200px
   :height: 100px
   :scale: 50 %
   :alt: Picture Text
   :align: right

This is some text placed next to a picture. The image is aligned to the 
right of this text.

That should be interesting enough.

* One
* Two
* Three

Even More Background
----------------------
This Document is put together using Sphinx & |RST|_

    If you want to learn about `my favorite programming language`_?

    .. _my favorite programming language: http://www.python.org

