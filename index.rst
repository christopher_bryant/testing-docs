.. Chris Bryant Testing documentation master file

Welcome to Chris Bryant's Demo documentation!
================================================

I am able to write/collate documentation for your project. Using the sphinx documentation system.

   Further information & Documentation can be found on there website.
   
   Click for more information on `Sphinx`_. 
   
   .. _Sphinx: https://www.sphinx-doc.org/

My Personal Website
-------------------
Feel free to visit my personal website `ChrisBryant.me.uk`_ for more information on network and internet based services that I offer.

.. _ChrisBryant.me.uk: https://chrisbryant.me.uk

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   background


